/*
 * positionFusion.h
 *
 *  Created on: Dec 15, 2015
 *      Author: Martin Chung
 *      Fuses low (accurate) position data and high (prone to drift) frequency acceleration data to get more accurate high frequency estimates.
 *      Uses a Kalman filter to fuse sensor information and a Bezier curve trapolation technique to get high frequency position estimates.
 *      Note: The time interval spanning the slow measurement buffer cannot exceed half of the allowable measurable time interval of the operating system.
 *
 */

#ifndef POSITIONFUSION_H_
#define POSITIONFUSION_H_
#include "arm_math.h"
#include "stdbool.h"

/**
 *
 */
#define MAX_NUMBER_OF_POSITION_MEASUREMENTS 5
#define MAX_POSITION_MEASUREMENT_INTERVAL 4000
#define KALMAN_UPDATE_INTERVAL 0.002

typedef struct {
	uint32_t tickFreq;
	float positionMeasurements[MAX_NUMBER_OF_POSITION_MEASUREMENTS];
	uint32_t positionMeasurementTimes[MAX_NUMBER_OF_POSITION_MEASUREMENTS];
	int positionMeasurementIndex;
	bool positionMeasurementsTaken;
	uint32_t latestTime;
	float w;
	float n;
	float estimate;
	float processMatrixData[9];
	arm_matrix_instance_f32 processMatrix; //F
	float observationMatrixData[9];
	arm_matrix_instance_f32 observationMatrix; //H
	float errorCovarianceMatrixData[9];
	arm_matrix_instance_f32 errorCovarianceMatrix; //P
	float errorMatrixData[9];
	arm_matrix_instance_f32 errorMatrix; //Q
	float kalmanGainMatrixData[9];
	arm_matrix_instance_f32 kalmanGainMatrix; //K
	float measurmentNoiseMatrixData[9];
	arm_matrix_instance_f32 measurmentNoiseMatrix; //R

	//Matrices for prediction phase calculations.
	float XPredData[3]; //X (predict)
	arm_matrix_instance_f32 XPred; //X (predict)
	float FtransposeData[9]; //F(transpose)
	arm_matrix_instance_f32 Ftranspose; //F(transpose)
	float FPData[9];  //F*P
	arm_matrix_instance_f32 FP; //F*P
	float FPFtransposeData[9]; //F*P*F(transpose)
	arm_matrix_instance_f32 FPFtranspose; //F*P*F(transpose)
	float FXPredData[3]; //F*X (Prediction phase)
	arm_matrix_instance_f32 FXPred; //F*X (Prediction phase)
	float PPredData[9]; //P (Prediction phase)
	arm_matrix_instance_f32 PPred; //P (Prediction phase)
	//Matrices for update phase calculations.
	float YData[3]; //y
	arm_matrix_instance_f32 Y; //y
	float HXData[3]; //H * XPred
	arm_matrix_instance_f32 HX; //H * XPred
	float HtransposeData[9]; //H(transpose)
	arm_matrix_instance_f32 Htranspose; //H(transpose)
	float HPData[9]; //H * P
	arm_matrix_instance_f32 HP; //H * P
	float HPHtransposeData[9]; //H * P * H(transpose)
	arm_matrix_instance_f32 HPHtranspose; //H * P * H(transpose)
	float SData[9]; //S
	arm_matrix_instance_f32 S; //S
	float InvSData[9]; //S (inverse)
	arm_matrix_instance_f32 InvS; //S (inverse)
	float PHtransposeData[9]; //P * H(transpose)
	arm_matrix_instance_f32 PHtranspose; //P * H (transpose)
	float KYData[3]; //K * y
	arm_matrix_instance_f32 KY; //K * y
	float IData[9]; //I
	arm_matrix_instance_f32 I; //I
	float KHData[9]; //K * H
	arm_matrix_instance_f32 KH; //K * H
	float IminusKHData[9]; // I - K * H
	arm_matrix_instance_f32 IminusKH; // I - K * H
	float XUpdData[3]; //X (update)
	arm_matrix_instance_f32 XUpd; //X (update)
	float ZData[3]; //Z
	arm_matrix_instance_f32 ZUpd; //Z
} PosFusionObj;

/**
 * @brief Initializes all matrices in the PosFusionObj for calculation.
 * @param[in,out] *obj           instance to the position fusion states.
 * @return        none
 *
 */
void initPosFusion(PosFusionObj* obj, uint32_t tickFreq);
/**
 * @brief Predicts and updates the Kalman filter with the acceleration reading and position estimate.
 * @param[in,out] *obj           instance to the position fusion states.
 * @param[in]     measurement    acceleration (high frequency) measurement (vertical component only).
 * @param[in]     time           system time of the measurement.
 * @return        none
 *
 */
void accelerationMeasure(PosFusionObj* obj, float measurement, uint32_t time);
/**
 * @brief Low frequency position measurement goes here.
 * @param[in,out] *obj           instance to the position fusion states.
 * @param[in]     measurement    position (low frequency) measurement (vertical component only).
 * @param[in]     time           system time of the measurement. Interval between measurements cannot exceed MAX_POSITION_MEASUREMENT_INTERVAL or the position measurements will reset.
 * @return        none
 *
 */
void positionMeasure(PosFusionObj* obj, float measurement, uint32_t time);
/**
 * @brief Get the position (vertical) estimate.
 * @param[in,out] *obj           instance to the position fusion states.
 * @return        vertical position estimate.
 *
 */
float getPositionEstimate(PosFusionObj* obj);
/**
 * @brief Get the velocity (vertical) estimate.
 * @param[in,out] *obj           instance to the position fusion states.
 * @return        vertical velocity estimate.
 *
 */
float getVelocityEstimate(PosFusionObj* obj);
/**
 * @brief Get the acceleration (vertical) estimate.
 * @param[in,out] *obj           instance to the position fusion states.
 * @return        vertical acceleration estimate.
 *
 */
float getAccelerationEstimate(PosFusionObj* obj);
#endif /* POSITIONFUSION_H_ */

