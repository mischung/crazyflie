/*
 * adaptiveController.c
 *
 *  Created on: Nov 24, 2015
 *      Author: Martin Chung
 */
#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "arm_math.h"
#include "controller.h"
#include "pid.h"
#include "param.h"
#include "imu.h"
#include "l1Piecewise.h"
#include "adaptiveController.h"
#include "pm.h"
#define HEIGHT_CONTROL

#ifndef UINT16_MAX
#define UINT16_MAX 65535
#endif
#undef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#undef CLAMP
#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })

static bool launching = false;
static float launchVelocity = 0.0f;
static L1Object angularRate;
static bool isInit = false;
static float32_t rateAmData[16] = { -5.0f, 0.0f, 0.0f, 0.0f, 0.0f, -5.0f, 0.0f,
		0.0f, 0.0f, 0.0f, -5.0f, 0.0f, 0.0f, 0.0f, 0.0f, -0.1f };
static float32_t rateBmData[16] = { -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 0.1f,
		0.1f, 0.1f, 0.1f };
static float32_t rateCmData[16] = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
static float32_t zState[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 rateAm = { 4, 4, rateAmData };
static arm_matrix_instance_f32 rateBm = { 4, 4, rateBmData };
static arm_matrix_instance_f32 rateCm = { 4, 4, rateCmData };
static arm_matrix_instance_f32 state = { 4, 1, zState };
static float32_t desiredState[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 desiredAngularRate = { 4, 1, desiredState };
static float32_t measuredState[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 measuredAngularRate = { 4, 1, measuredState };
static float32_t deadZoneData[4] = { 0.3f, 0.3f, 0.3f, 0.007f };
static arm_matrix_instance_f32 deadZone = { 4, 1, deadZoneData };
static float angularRateOmega = 1000.0f;
static float verticalRateOmega = 1000.0f;
static float verticalRate = 0.0f;
static float32_t uMinData[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
static float32_t uMaxData[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 uMin = { 4, 1, uMinData };
static arm_matrix_instance_f32 uMax = { 4, 1, uMaxData };

#ifdef HEIGHT_CONTROL

static L1SISOObject verticalRateObj;
static float32_t vRateAm = -5.0f;
static float32_t vRateBm = 1.0f;
static float32_t vRateCm = 1.0f;
static float32_t vzState = 0.0f;
static float32_t vDeadZone = 0.007f;
static float32_t vUMin = 0.0f;
static float32_t vUMax = 0.0f;
#endif

PidObject pidRoll;
PidObject pidPitch;
PidObject pidYaw;
PidObject pidHeight;
PidObject pidHeightRate;
uint16_t motorPowerM1;
uint16_t motorPowerM2;
uint16_t motorPowerM3;
uint16_t motorPowerM4;
size_t freeHeapSize;

static inline int16_t saturateSignedInt16(float in) {
	// don't use INT16_MIN, because later we may negate it, which won't work for that value.
	if (in > INT16_MAX)
		return INT16_MAX;
	else if (in < -INT16_MAX)
		return -INT16_MAX;
	else
		return (int16_t) in;
}

void adaptiveControllerInit(void) {
	if (isInit)
		return;
	l1Init(&angularRate, &state, &rateAm, &rateBm, &rateCm, angularRateOmega,
			ADAPTIVE_UPDATE_DT);
	pidInit(&pidRoll, 0, PID_ROLL_KP, PID_ROLL_KI, PID_ROLL_KD,
			ADAPTIVE_UPDATE_DT);
	pidInit(&pidPitch, 0, PID_PITCH_KP, PID_PITCH_KI, PID_PITCH_KD,
	ADAPTIVE_UPDATE_DT);
	pidInit(&pidYaw, 0, PID_YAW_KP, PID_YAW_KI, PID_YAW_KD, ADAPTIVE_UPDATE_DT);
	pidInit(&pidHeight, 0, PID_HEIGHT_KP, PID_HEIGHT_KI, PID_HEIGHT_KD,
			ADAPTIVE_UPDATE_DT);
	pidSetIntegralLimit(&pidRoll, PID_ROLL_INTEGRATION_LIMIT);
	pidSetIntegralLimit(&pidPitch, PID_PITCH_INTEGRATION_LIMIT);
	pidSetIntegralLimit(&pidYaw, PID_YAW_INTEGRATION_LIMIT);
	pidSetIntegralLimit(&pidHeight, PID_HEIGHT_INTEGRATION_LIMIT);
#ifdef HEIGHT_CONTROL
	l1InitSISO(&verticalRateObj, vzState, vRateAm, vRateBm, vRateCm, verticalRateOmega,
	ADAPTIVE_UPDATE_DT);
	//pidInit(&pidHeightRate, 0, PID_HEIGHT_RATE_KP, PID_HEIGHT_RATE_KI, PID_HEIGHT_RATE_KD,
	//		ADAPTIVE_UPDATE_DT);
	//pidSetIntegralLimit(&pidHeightRate, PID_HEIGHT_RATE_INTEGRATION_LIMIT);
#endif

	isInit = true;
}

bool adaptiveControllerTest() {
	return isInit;
}
void adaptiveControllerCorrectRate(float rollRateActual, float pitchRateActual,
		float yawRateActual, float verticalVelocityActual,
		float rollRateDesired, float pitchRateDesired, float yawRateDesired,
		float verticalVelocityDesired) {
	float thrust = 0.0f;
	float powerRatio = MAX((pmGetBatteryVoltage() - 1.5f)/2.7f, 0.25f);
	desiredAngularRate.pData[0] = rollRateDesired;
	desiredAngularRate.pData[1] = pitchRateDesired;
	desiredAngularRate.pData[2] = yawRateDesired;
	measuredAngularRate.pData[0] = rollRateActual;
	measuredAngularRate.pData[1] = pitchRateActual;
	measuredAngularRate.pData[2] = yawRateActual;
	measuredAngularRate.pData[3] = 0.0f;
	desiredAngularRate.pData[3] = 0.0f;
#ifdef HEIGHT_CONTROL
//	desiredVerticalRate.pData[0] = verticalVelocityDesired;
//	measuredVerticalRate.pData[0] = verticalVelocityActual;
//	measuredAngularRate.pData[3] = verticalVelocityActual;
//	desiredAngularRate.pData[3] = verticalVelocityDesired;
	// Update PID for height rate
	//pidSetDesired(&pidHeightRate, verticalVelocityDesired);
	//thrust = pidUpdate(&pidHeightRate, verticalVelocityActual, true) * 256.0f / powerRatio;
	//thrust = CLAMP(thrust, 0.0f, 256.0f * 0.9f);
#endif
	uMin.pData[0] = 0.0f;
	uMin.pData[1] = 0.0f;
	uMin.pData[2] = 0.0f;
	uMin.pData[3] = 0.0f;
#ifdef HEIGHT_CONTROL
//	uMax.pData[0] = 256.0f - verticalRate.u.pData[0];
//	uMax.pData[1] = 256.0f - verticalRate.u.pData[0];
//	uMax.pData[2] = 256.0f - verticalRate.u.pData[0];
//	uMax.pData[3] = 256.0f - verticalRate.u.pData[0];
//	uMax.pData[0] = (256.0f * 0.5f);
//	uMax.pData[1] = (256.0f * 0.5f);
//	uMax.pData[2] = (256.0f * 0.5f);
//	uMax.pData[3] = (256.0f * 0.5f);
//	uMax.pData[0] = 256.0f;
//	uMax.pData[1] = 256.0f;
//	uMax.pData[2] = 256.0f;
//	uMax.pData[3] = 256.0f;
	uMax.pData[0] = 255.0f * 0.3f;// - verticalVelocityDesired;
	uMax.pData[1] = 255.0f * 0.3f;//- verticalVelocityDesired;
	uMax.pData[2] = 255.0f * 0.3f;//- verticalVelocityDesired;
	uMax.pData[3] = 255.0f * 0.3f;//- verticalVelocityDesired;
#else
	uMax.pData[0] = MIN(256.0f * 0.3f, 256.0f - verticalVelocityDesired * 0.8f);// - verticalVelocityDesired;
	uMax.pData[1] = MIN(256.0f * 0.3f, 256.0f - verticalVelocityDesired * 0.8f);//- verticalVelocityDesired;
	uMax.pData[2] = MIN(256.0f * 0.3f, 256.0f - verticalVelocityDesired * 0.8f);//- verticalVelocityDesired;
	uMax.pData[3] = MIN(256.0f * 0.3f, 256.0f - verticalVelocityDesired * 0.8f);//- verticalVelocityDesired;
#endif
	l1SetDesired(&angularRate, &desiredAngularRate);
	l1Update(&angularRate, &measuredAngularRate, &uMin, &uMax,
			&deadZone);

#ifdef HEIGHT_CONTROL
	float approxExtraThrust = CLAMP((angularRate.u.pData[0] + angularRate.u.pData[1] + angularRate.u.pData[2] + angularRate.u.pData[3]) / 4.0f, 0.0f, 256.0f);
	vUMin = approxExtraThrust;
	vUMax = 0.7f * 256.0f + approxExtraThrust; //Motor power dedicated to vertical thrust.
	l1SetDesiredSISO(&verticalRateObj, verticalVelocityDesired);
	l1UpdateSISO(&verticalRateObj, verticalVelocityActual, vUMin, vUMax,
			vDeadZone);
	thrust = verticalRateObj.u;// / powerRatio;
	thrust = CLAMP(thrust - approxExtraThrust, 0.0f, 256.0f * 0.7f);
//	motorPowerM1 = CLAMP((angularRate.u.pData[0]) * 256.0f, 0.0f , (float)UINT16_MAX);
//	motorPowerM2 = CLAMP((angularRate.u.pData[1]) * 256.0f, 0.0f , (float)UINT16_MAX);
//	motorPowerM3 = CLAMP((angularRate.u.pData[2]) * 256.0f, 0.0f , (float)UINT16_MAX);
//	motorPowerM4 = CLAMP((angularRate.u.pData[3]) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM1 = CLAMP((angularRate.u.pData[0] + thrust) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM2 = CLAMP((angularRate.u.pData[1] + thrust) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM3 = CLAMP((angularRate.u.pData[2] + thrust) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM4 = CLAMP((angularRate.u.pData[3] + thrust) * 256.0f, 0.0f , (float)UINT16_MAX);
/*	motorPowerM1 = angularRate.u.pData[0] * 256.0f;
	motorPowerM2 = angularRate.u.pData[1] * 256.0f;
	motorPowerM3 = angularRate.u.pData[2] * 256.0f;
	motorPowerM4 = angularRate.u.pData[3] * 256.0f;
	*/
#else
	motorPowerM1 = CLAMP((angularRate.u.pData[0] + verticalVelocityDesired * 0.8f) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM2 = CLAMP((angularRate.u.pData[1] + verticalVelocityDesired * 0.8f) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM3 = CLAMP((angularRate.u.pData[2] + verticalVelocityDesired * 0.8f) * 256.0f, 0.0f , (float)UINT16_MAX);
	motorPowerM4 = CLAMP((angularRate.u.pData[3] + verticalVelocityDesired * 0.8f) * 256.0f, 0.0f , (float)UINT16_MAX);

#endif
}
void adaptiveControllerCorrectAttitude(float eulerRollActual,
		float eulerPitchActual, float eulerYawActual, float heightActual,
		float eulerRollDesired, float eulerPitchDesired, float eulerYawDesired,
		float heightDesired, float* rollRateDesired, float* pitchRateDesired,
		float* yawRateDesired, float *verticalVelocityDesired) {
	pidSetDesired(&pidRoll, eulerRollDesired);
	*rollRateDesired = pidUpdate(&pidRoll, eulerRollActual, true);

	// Update PID for pitch axis
	pidSetDesired(&pidPitch, eulerPitchDesired);
	*pitchRateDesired = pidUpdate(&pidPitch, eulerPitchActual, true);


	// Update PID for yaw axis
	float yawError;
	yawError = eulerYawDesired - eulerYawActual;
	if (yawError > 180.0)
		yawError -= 360.0;
	else if (yawError < -180.0)
		yawError += 360.0;
	pidSetError(&pidYaw, yawError);
	*yawRateDesired = pidUpdate(&pidYaw, eulerYawActual, false);

//	if (launching)
//	{
//		verticalRate = verticalRateOmega * launchVelocity + (1.0f - verticalRateOmega) * verticalRate;
//		*verticalVelocityDesired = verticalRate;
//	}
//	else
//	{
		// Update PID for height
		pidSetDesired(&pidHeight, heightDesired);
		verticalRate = pidUpdate(&pidHeight, heightActual, true);//verticalRateOmega * pidUpdate(&pidHeight, heightActual, true) + (1.0f - verticalRateOmega) * verticalRate;
		*verticalVelocityDesired = verticalRate;//CLAMP(verticalRate, -1.0f, 1.0f);
//	}

}

void adaptiveControllerGetActuatorOutput(uint16_t* m1, uint16_t* m2,
		uint16_t* m3, uint16_t* m4) {
	*m1 = motorPowerM1;
	*m2 = motorPowerM2;
	*m3 = motorPowerM3;
	*m4 = motorPowerM4;
}
void adaptiveControllerResetAll(void) {
	pidReset(&pidRoll);
	pidReset(&pidPitch);
	pidReset(&pidYaw);
	pidReset(&pidHeight);
	l1Reset(&angularRate);
#ifdef HEIGHT_CONTROL
	l1ResetSISO(&verticalRateObj);
#endif
}

void startLaunch(float velocity) {
	if (!launching)
	{
		launching = true;
		l1Reset(&angularRate);
		pidReset(&pidHeightRate);
	}
	launchVelocity = velocity;//verticalRateOmega * velocity + (1.0f - verticalRateOmega) * launchVelocity;
}

void stopLaunch(void) {
	if (launching)
	{
		launching = false;
		l1Reset(&angularRate);
		pidReset(&pidHeightRate);
	}
	launchVelocity = 0.0f;
}

bool isLaunching(void) {
	return launching;
}
