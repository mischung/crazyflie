/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie Firmware
 *
 * Copyright (C) 2011-2012 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#ifndef ADAPTIVE_CONTROLLER
#define ADAPTIVE_CONTROLLER
#endif
#define HEIGHT_CONTROL

#include <math.h>

#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOSConfig.h"
#include "config.h"
#include "system.h"
#include "pm.h"
#include "stabilizer.h"
#include "commander.h"
#ifdef ADAPTIVE_CONTROLLER
#include "adaptiveController.h"
#include "pwm_capture_mb10x3.h"
#include "positionFusion.h"
#include "vl6180x.h"
#else
#include "controller.h"
#endif
#include "sensfusion6.h"
#include "imu.h"
#include "motors.h"
#include "log.h"
#include "pid.h"
#include "ledseq.h"
#include "param.h"
//#include "ms5611.h"
#include "lps25h.h"
#include "debug.h"

#undef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#undef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#undef CLAMP
#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })
/**
 * Defines in what divided update rate should the attitude
 * control loop run relative the rate control loop.
 */
#define QUAD_FORMATION_X
//#define MAGNETOMETER_CALIBRATION
#define ATTITUDE_UPDATE_RATE_DIVIDER  2
#define FUSION_UPDATE_DT  (float)(1.0 / (IMU_UPDATE_FREQ / ATTITUDE_UPDATE_RATE_DIVIDER)) // 500hz

// Barometer/ Altitude hold stuff
#define ALTHOLD_UPDATE_RATE_DIVIDER  5 // 500hz/5 = 100hz for barometer measurements
#define ALTHOLD_UPDATE_DT  (float)(1.0 / (IMU_UPDATE_FREQ / ALTHOLD_UPDATE_RATE_DIVIDER))   // 1000hz

#define ACCELERATION_GRAVITY 9.81

static Axis3f gyro; // Gyro axis data in deg/s
static Axis3f acc;  // Accelerometer axis data in mG
static Axis3f mag;  // Magnetometer axis data in testla

static float eulerRollActual;
static float eulerPitchActual;
static float eulerYawActual;
static float eulerRollDesired;
static float eulerPitchDesired;
static float eulerYawDesired;
static float rollRateDesired;
static float pitchRateDesired;
static float yawRateDesired;
#ifdef ADAPTIVE_CONTROLLER
static float heightDesired = 0.0f;
static float heightActual = 0.0f;
static float verticalVelocityDesired = 0.0f;
static float lastHeight = 0.0f;
static float vertAccBias = 0.0f; //in ms-2
static float thisGravityMag = 0.0f; //in g's.
static bool gravityMagInitialized = false;
#endif

// Baro variables
static float temperature; // temp from barometer
static float pressure;    // pressure from barometer
static float asl;     // smoothed asl
static float aslRaw;  // raw asl
static float aslLong; // long term asl

// Altitude hold variables
static PidObject altHoldPID; // Used for altitute hold mode. I gets reset when the bat status changes
bool altHold = false;          // Currently in altitude hold mode
bool setAltHold = false;      // Hover mode has just been activated
static float accWZ = 0.0;
static float accMAG = 0.0;
static float vSpeedASL = 0.0;
static float vSpeedAcc = 0.0;
static float vSpeed = 0.0; // Vertical speed (world frame) integrated from vertical acceleration
static float altHoldPIDVal;                    // Output of the PID controller
static float altHoldErr;        // Different between target and current altitude

// Altitude hold & Baro Params
static float altHoldKp = 0.5; // PID gain constants, used everytime we reinitialise the PID controller
static float altHoldKi = 0.18;
static float altHoldKd = 0.0;
static float altHoldChange = 0;     // Change in target altitude
static float altHoldTarget = -1;    // Target altitude
static float altHoldErrMax = 1.0; // max cap on current estimated altitude vs target altitude in meters
static float altHoldChange_SENS = 200; // sensitivity of target altitude change (thrust input control) while hovering. Lower = more sensitive & faster changes
static float pidAslFac = 13000; // relates meters asl to thrust
static float pidAlpha = 0.8;   // PID Smoothing //TODO: shouldnt need to do this
static float vSpeedASLFac = 0;    // multiplier
static float vSpeedAccFac = -48;  // multiplier
#ifdef ADAPTIVE_CONTROLLER
static float vAccDeadband = 0.025;  // Vertical acceleration deadband
static float vSpeedLimit = 1.0;  // used to constrain vertical velocity
static PosFusionObj heightFusion;
//static float rollIntegral = 0;
//static float pitchIntegral = 0;
//static float floorHeight = 0;
static float estHeight = 0.0f;
static float estVelocity = 0.0f;
#else
static float vAccDeadband = 0.05;  // Vertical acceleration deadband
static float vSpeedLimit = 0.05;// used to constrain vertical velocity
#endif
static float vSpeedASLDeadband = 0.005; // Vertical speed based on barometer readings deadband
static float errDeadband = 0.00;  // error (target - altitude) deadband
static float vBiasAlpha = 0.98; // Blending factor we use to fuse vSpeedASL and vSpeedAcc
static float aslAlpha = 0.92; // Short term smoothing
static float aslAlphaLong = 0.93; // Long term smoothing
static uint16_t altHoldMinThrust = 00000; // minimum hover thrust - not used yet
static uint16_t altHoldBaseThrust = 43000; // approximate throttle needed when in perfect hover. More weight/older battery can use a higher value
static uint16_t altHoldMaxThrust = 60000; // max altitude hold thrust
RPYType rollType;
RPYType pitchType;
RPYType yawType;

uint16_t actuatorThrust;
#ifdef ADAPTIVE_CONTROLLER
uint16_t motorPowerM4;
uint16_t motorPowerM2;
uint16_t motorPowerM1;
uint16_t motorPowerM3;
#else

int16_t actuatorRoll;
int16_t actuatorPitch;
int16_t actuatorYaw;
uint32_t motorPowerM4;
uint32_t motorPowerM2;


uint32_t motorPowerM1;
uint32_t motorPowerM3;
#endif

static bool isInit;

static void stabilizerAltHoldUpdate(void);

#ifdef ADAPTIVE_CONTROLLER
static void distributePower(const uint16_t m1, const uint16_t m2,
		const uint16_t m3, const uint16_t m4);
static float getHeightMeasurement();
#else
static void distributePower(const uint16_t thrust, const int16_t roll,
		const int16_t pitch, const int16_t yaw);
#endif
static uint16_t limitThrust(int32_t value);
static void stabilizerTask(void* param);
static float constrain(float value, const float minVal, const float maxVal);
static float deadband(float value, const float threshold);

void stabilizerInit(void) {
	if (isInit)
		return;

	motorsInit();
	imu6Init();
	sensfusion6Init();
#ifdef ADAPTIVE_CONTROLLER
	adaptiveControllerInit();
	initPosFusion(&heightFusion, configTICK_RATE_HZ);
	pwmMB10X3Init();
	vl6180xInit();

#else
	controllerInit();
#endif

	rollRateDesired = 0;
	pitchRateDesired = 0;
	yawRateDesired = 0;

	xTaskCreate(stabilizerTask, (const signed char * const)STABILIZER_TASK_NAME,
			STABILIZER_TASK_STACKSIZE, NULL, STABILIZER_TASK_PRI, NULL);

	isInit = true;
}

bool stabilizerTest(void) {
	bool pass = true;

	pass &= motorsTest();
	pass &= imu6Test();
	pass &= sensfusion6Test();
#ifdef ADAPTIVE_CONTROLLER
	pass &= adaptiveControllerTest();
	pass &= pwmMB10X3Test();
    pass &= vl6180xSelfTest();
#else
	pass &= controllerTest();
#endif
	return pass;
}

static void stabilizerTask(void* param) {
#ifdef ADAPTIVE_CONTROLLER
	uint32_t imuCounter = 0;
#endif
	uint32_t attitudeCounter = 0;
	uint32_t altHoldCounter = 0;
	uint32_t lastWakeTime;

	vTaskSetApplicationTaskTag(0, (void*) TASK_STABILIZER_ID_NBR);

	//Wait for the system to be fully started to start stabilization loop
	systemWaitStart();
	lastWakeTime = xTaskGetTickCount();

	while (1) {
#ifdef ADAPTIVE_CONTROLLER
		vTaskDelayUntil(&lastWakeTime, F2T(ADAPTATION_FREQUENCY)); // 1000Hz
		if (++imuCounter >= ADAPTATION_FREQUENCY / IMU_UPDATE_FREQ) { // 1000Hz
			imu9Read(&gyro, &acc, &mag);
			imuCounter = 0;
		}
#else
		vTaskDelayUntil(&lastWakeTime, F2T(IMU_UPDATE_FREQ)); // 500Hz
		// Magnetometer not yet used more then for logging.
		imu9Read(&gyro, &acc, &mag);
#endif

		if (imu6IsCalibrated()) {
			commanderGetRPY(&eulerRollDesired, &eulerPitchDesired,
					&eulerYawDesired);
			commanderGetRPYType(&rollType, &pitchType, &yawType);

#ifdef ADAPTIVE_CONTROLLER
			//10Hz
			if (++altHoldCounter > ADAPTATION_FREQUENCY / MB10x3_UPDATE_FREQ) {
				heightActual = getHeightMeasurement();
				if (heightActual > 0.0f)
					positionMeasure(&heightFusion, heightActual, lastWakeTime);
			}
			// 500HZ
			float accZ;
			if (++attitudeCounter
					>= ATTITUDE_UPDATE_RATE_DIVIDER * ADAPTATION_FREQUENCY
							/ IMU_UPDATE_FREQ / 2) {
				sensfusion6UpdateQ(gyro.x, gyro.y, gyro.z, acc.x, acc.y, acc.z,
				FUSION_UPDATE_DT );
				sensfusion6GetEulerRPY(&eulerRollActual, &eulerPitchActual,
						&eulerYawActual);
				accZ = sensfusion6GetAccZWithoutGravity(acc.x, acc.y, acc.z)
						+ 1.0f;
				// Assumes the craft is placed upright at a stable position immediately after imu calibration.
				if (!gravityMagInitialized) {
					thisGravityMag = accZ;
					gravityMagInitialized = true;
				}
//				if (motorPowerM1 == 0 && motorPowerM2 == 0 && motorPowerM3 == 0 && motorPowerM4 == 0)
//				{
//					floorHeight = 0.9f * floorHeight + 0.1f * heightActual;
//				}
//				if (getPositionEstimate(&heightFusion) > floorHeight + 0.005)
//				{
					//rollIntegral += eulerRollActual * FUSION_UPDATE_DT;
					//pitchIntegral += eulerPitchActual * FUSION_UPDATE_DT;
//					if (rollIntegral > 1.0f)
//						rollIntegral = 0.0f;
//					if (pitchIntegral > 1.0f)
//						pitchIntegral = 0.0f;
//				}
//				else
//				{
//					rollIntegral = 0.0f;
//					pitchIntegral = 0.0f;
//				}
				accWZ = (accZ - thisGravityMag) * ACCELERATION_GRAVITY;
				// Estimate speed from acc (drifts)
				accelerationMeasure(&heightFusion, accWZ, lastWakeTime);
				vertAccBias += accWZ - getAccelerationEstimate(&heightFusion);
				commanderGetThrust(&actuatorThrust);
				if (commanderGetInactivityTime() > COMMANDER_WDT_TIMEOUT_STABALIZE) {
					heightDesired = 0.0f;
				} else {
					heightDesired = 0.45f;//max(0.4f,
							//(float )actuatorThrust / 65535.0f);
				}
				adaptiveControllerCorrectAttitude(eulerRollActual,
						eulerPitchActual, eulerYawActual, getPositionEstimate(&heightFusion),
						eulerRollDesired, eulerPitchDesired, -eulerYawDesired,
						heightDesired, &rollRateDesired, &pitchRateDesired,
						&yawRateDesired, &verticalVelocityDesired);
				attitudeCounter = 0;
			}
#else
			// 250HZ
			if (++attitudeCounter >= ATTITUDE_UPDATE_RATE_DIVIDER) {
				sensfusion6UpdateQ(gyro.x, gyro.y, gyro.z, acc.x, acc.y, acc.z,
						FUSION_UPDATE_DT);
				sensfusion6GetEulerRPY(&eulerRollActual, &eulerPitchActual,
						&eulerYawActual);
				accWZ = sensfusion6GetAccZWithoutGravity(acc.x, acc.y, acc.z);
				accMAG = (acc.x * acc.x) + (acc.y * acc.y) + (acc.z * acc.z);
				// Estimate speed from acc (drifts)
				vSpeed += deadband(accWZ, vAccDeadband) * FUSION_UPDATE_DT;
				controllerCorrectAttitudePID(eulerRollActual, eulerPitchActual,
						eulerYawActual, eulerRollDesired, eulerPitchDesired,
						-eulerYawDesired, &rollRateDesired, &pitchRateDesired,
						&yawRateDesired);
				attitudeCounter = 0;
			}
#endif
#ifdef ADAPTIVE_CONTROLLER
			//10Hz
			if (++altHoldCounter > ADAPTATION_FREQUENCY / MB10x3_UPDATE_FREQ) {
				heightActual = getHeightMeasurement();
				thisGravityMag += vertAccBias / (ACCELERATION_GRAVITY * IMU_UPDATE_FREQ / MB10x3_UPDATE_FREQ);
				//vertAccBias = 0.0f;
#ifdef HEIGHT_CONTROL
				// Update only when height is within reading range.
				//if (heightActual > 0.31f && heightActual < 5.0f
				//		&& lastHeight > 0.31f && lastHeight < 5.0f) {
				//	vSpeed = 1.0f * (heightActual - lastHeight)
				//			/ MB10x3_UPDATE_FREQ;// + 0.1f * vSpeed;
				//	stopLaunch();
				//}
				//else
				//{
				//	if (heightDesired > 0.3f && heightDesired < 5.0f) {
				//		//startLaunch(0.01f); //Launch
				//		startLaunch(1.0f); //Launch
				//	} else {
				//		//startLaunch(-0.01f); //Land
				//		startLaunch(-1.0f); //Land
				//	}
				//}
				//lastHeight = heightActual;
				// Stop operation if height exceeds specific amount.
				if (heightActual > 0.9f) {
					distributePower(0, 0, 0, 0);
					adaptiveControllerResetAll();
					if (commanderGetInactivityTime() <= COMMANDER_WDT_TIMEOUT_STABALIZE) {
						ASSERT(false);
					}
				}
#endif
				altHoldCounter = 0;
			}
			// 100HZ
			/*			if (imuHasBarometer()
			 && (++altHoldCounter
			 >= ALTHOLD_UPDATE_RATE_DIVIDER
			 * ADAPTATION_FREQUENCY / IMU_UPDATE_FREQ)) {
			 stabilizerAltHoldUpdate();
			 altHoldCounter = 0;
			 } */
#else
			// 100HZ
			if (imuHasBarometer()
					&& (++altHoldCounter >= ALTHOLD_UPDATE_RATE_DIVIDER)) {
				stabilizerAltHoldUpdate();
				altHoldCounter = 0;
			}
#endif

			if (rollType == RATE) {
				rollRateDesired = eulerRollDesired;
			}
			if (pitchType == RATE) {
				pitchRateDesired = eulerPitchDesired;
			}
			if (yawType == RATE) {
				yawRateDesired = -eulerYawDesired;
			}

			if (!altHold || !imuHasBarometer()) {
				// Use thrust from controller if not in altitude hold mode
				commanderGetThrust(&actuatorThrust);
			} else {
				// Added so thrust can be set to 0 while in altitude hold mode after disconnect
				commanderWatchdog();
			}
			//if (commanderGetInactivityTime() <= COMMANDER_WDT_TIMEOUT_STABALIZE) {
			//	yawRateDesired = 400.0f;
			//}
			// TODO: Investigate possibility to subtract gyro drift.
#ifdef ADAPTIVE_CONTROLLER
			estVelocity = getVelocityEstimate(&heightFusion);
			estHeight = getPositionEstimate(&heightFusion);

#ifdef HEIGHT_CONTROL
			adaptiveControllerCorrectRate(gyro.x, gyro.y, gyro.z, getVelocityEstimate(&heightFusion),
					rollRateDesired, pitchRateDesired, yawRateDesired,
					verticalVelocityDesired);
#else
			adaptiveControllerCorrectRate(gyro.x, gyro.y, gyro.z, getVelocityEstimate(&heightFusion),
					rollRateDesired, pitchRateDesired, yawRateDesired,
					(float) actuatorThrust / 256.0f);
#endif
			adaptiveControllerGetActuatorOutput(&motorPowerM1, &motorPowerM2,
					&motorPowerM3, &motorPowerM4);
#else
			controllerCorrectRatePID(gyro.x, gyro.y, gyro.z, rollRateDesired,
					pitchRateDesired, yawRateDesired);
			controllerGetActuatorOutput(&actuatorRoll, &actuatorPitch,
					&actuatorYaw);
#endif
#ifdef ADAPTIVE_CONTROLLER
#ifdef HEIGHT_CONTROL
			if (commanderGetInactivityTime() > COMMANDER_WDT_TIMEOUT_STABALIZE) {
#else
				if (actuatorThrust <= 0 && commanderGetInactivityTime() > COMMANDER_WDT_TIMEOUT_STABALIZE) {
#endif
					//distributePower(motorPowerM1, motorPowerM2, motorPowerM3,
					//		motorPowerM4);
				distributePower(0, 0, 0, 0);
				adaptiveControllerResetAll();
//				rollIntegral = 0.0f;
//				pitchIntegral = 0.0f;
			} else {
				distributePower(motorPowerM1, motorPowerM2, motorPowerM3,
						motorPowerM4);
				//distributePower(0, 0, 0, 0);
				//				adaptiveControllerResetAll();
			}
#else
			if (actuatorThrust > 0) {
#if defined(TUNE_ROLL)
				distributePower(actuatorThrust, actuatorRoll, 0, 0);
#elif defined(TUNE_PITCH)
				distributePower(actuatorThrust, 0, actuatorPitch, 0);
#elif defined(TUNE_YAW)
				distributePower(actuatorThrust, 0, 0, -actuatorYaw);
#else
				distributePower(actuatorThrust, actuatorRoll, actuatorPitch,
						-actuatorYaw);
			} else {
				distributePower(0, 0, 0, 0);
				controllerResetAllPID();
			}
#endif
#endif
		}
	}
}
static void stabilizerAltHoldUpdate(void) {
	// Get altitude hold commands from pilot
	commanderGetAltHold(&altHold, &setAltHold, &altHoldChange);

	// Get barometer height estimates
	//TODO do the smoothing within getData
	lps25hGetData(&pressure, &temperature, &aslRaw);

	asl = asl * aslAlpha + aslRaw * (1 - aslAlpha);
	aslLong = aslLong * aslAlphaLong + aslRaw * (1 - aslAlphaLong);

	// Estimate vertical speed based on successive barometer readings. This is ugly :)
	vSpeedASL = deadband(asl - aslLong, vSpeedASLDeadband);

	// Estimate vertical speed based on Acc - fused with baro to reduce drift
	vSpeed = constrain(vSpeed, -vSpeedLimit, vSpeedLimit);
	vSpeed = vSpeed * vBiasAlpha + vSpeedASL * (1.f - vBiasAlpha);
	vSpeedAcc = vSpeed;

	// Reset Integral gain of PID controller if being charged
	if (!pmIsDischarging()) {
		altHoldPID.integ = 0.0;
	}

	// Altitude hold mode just activated, set target altitude as current altitude. Reuse previous integral term as a starting point
	if (setAltHold) {
		// Set to current altitude
		altHoldTarget = asl;

		// Cache last integral term for reuse after pid init
		const float pre_integral = altHoldPID.integ;

		// Reset PID controller
		pidInit(&altHoldPID, asl, altHoldKp, altHoldKi, altHoldKd,
		ALTHOLD_UPDATE_DT);
		// TODO set low and high limits depending on voltage
		// TODO for now just use previous I value and manually set limits for whole voltage range
		//                    pidSetIntegralLimit(&altHoldPID, 12345);
		//                    pidSetIntegralLimitLow(&altHoldPID, 12345);              /

		altHoldPID.integ = pre_integral;

		// Reset altHoldPID
		altHoldPIDVal = pidUpdate(&altHoldPID, asl, false);
	}

	// In altitude hold mode
	if (altHold) {
		// Update target altitude from joy controller input
		altHoldTarget += altHoldChange / altHoldChange_SENS;
		pidSetDesired(&altHoldPID, altHoldTarget);

		// Compute error (current - target), limit the error
		altHoldErr = constrain(deadband(asl - altHoldTarget, errDeadband),
				-altHoldErrMax, altHoldErrMax);
		pidSetError(&altHoldPID, -altHoldErr);

		// Get control from PID controller, dont update the error (done above)
		// Smooth it and include barometer vspeed
		// TODO same as smoothing the error??
		altHoldPIDVal = (pidAlpha) * altHoldPIDVal
				+ (1.f - pidAlpha)
						* ((vSpeedAcc * vSpeedAccFac)
								+ (vSpeedASL * vSpeedASLFac)
								+ pidUpdate(&altHoldPID, asl, false));

		// compute new thrust
		actuatorThrust =
				max(altHoldMinThrust,
						min(altHoldMaxThrust, limitThrust( altHoldBaseThrust + (int32_t)(altHoldPIDVal*pidAslFac))));

		// i part should compensate for voltage drop

	} else {
		altHoldTarget = 0.0;
		altHoldErr = 0.0;
		altHoldPIDVal = 0.0;
	}
}
#ifdef ADAPTIVE_CONTROLLER
static void distributePower(const uint16_t m1, const uint16_t m2,
		const uint16_t m3, const uint16_t m4) {
	motorsSetRatio(MOTOR_M1, limitThrust(m1));
	motorsSetRatio(MOTOR_M2, limitThrust(m2));
	motorsSetRatio(MOTOR_M3, limitThrust(m3));
	motorsSetRatio(MOTOR_M4, limitThrust(m4));
}
#else
static void distributePower(const uint16_t thrust, const int16_t roll,
		const int16_t pitch, const int16_t yaw) {
#ifdef QUAD_FORMATION_X
	int16_t r = roll >> 1;
	int16_t p = pitch >> 1;
//motorPowerM1 = limitThrust(thrust - r + p + yaw);
//motorPowerM2 = limitThrust(thrust - r - p - yaw);
//motorPowerM3 =  limitThrust(thrust + r - p + yaw);
//motorPowerM4 =  limitThrust(thrust + r + p - yaw);
	motorPowerM1 = limitThrust(thrust - r - p + yaw);
	motorPowerM2 = limitThrust(thrust + r - p - yaw);
	motorPowerM3 = limitThrust(thrust + r + p + yaw);
	motorPowerM4 = limitThrust(thrust - r + p - yaw);
#else // QUAD_FORMATION_NORMAL
//  motorPowerM1 = limitThrust(thrust + pitch + yaw);
//  motorPowerM2 = limitThrust(thrust - roll - yaw);
//  motorPowerM3 =  limitThrust(thrust - pitch + yaw);
//  motorPowerM4 =  limitThrust(thrust + roll - yaw);
	motorPowerM1 = limitThrust(thrust - pitch + yaw);
	motorPowerM2 = limitThrust(thrust + roll - yaw);
	motorPowerM3 = limitThrust(thrust + pitch + yaw);
	motorPowerM4 = limitThrust(thrust - roll - yaw);
#endif
#ifdef MAGNETOMETER_CALIBRATION
	motorPowerM1 = limitThrust(thrust);
	motorPowerM2 = limitThrust(thrust);
	motorPowerM3 = limitThrust(thrust);
	motorPowerM4 = limitThrust(thrust);
#endif
	motorsSetRatio(MOTOR_M1, motorPowerM1);
	motorsSetRatio(MOTOR_M2, motorPowerM2);
	motorsSetRatio(MOTOR_M3, motorPowerM3);
	motorsSetRatio(MOTOR_M4, motorPowerM4);
}
#endif
static uint16_t limitThrust(int32_t value) {
	if (value > UINT16_MAX) {
		value = UINT16_MAX;
	} else if (value < 0) {
		value = 0;
	}

	return (uint16_t) value;
}

// Constrain value between min and max
static float constrain(float value, const float minVal, const float maxVal) {
	return min(maxVal, max(minVal,value));
}

// Deadzone
static float deadband(float value, const float threshold) {
	if (fabs(value) < threshold) {
		value = 0;
	} else if (value > 0) {
		value -= threshold;
	} else if (value < 0) {
		value += threshold;
	}
	return value;
}

#ifdef ADAPTIVE_CONTROLLER
static float getHeightMeasurement()
{
	float vl6180Height = 0;
	float mb10x3Height = 0;
	mb10x3Height = mb10X3GetRange();
	vl6180Height = vl6180GetRange();
	if (vl6180Height < 0.2f && vl6180Height > 0.001f)
	{
		return vl6180Height;
	} else if (mb10x3Height > 0.31f && mb10x3Height < 4.9f)
	{
		return mb10x3Height;
	}
	else
	{
		return -1.0f;
	}
}
#endif

LOG_GROUP_START(stabilizer) LOG_ADD(LOG_FLOAT, roll, &eulerRollActual)
LOG_ADD(LOG_FLOAT, pitch, &eulerPitchActual)
LOG_ADD(LOG_FLOAT, yaw, &eulerYawActual)
LOG_ADD(LOG_UINT16, thrust, &actuatorThrust)
LOG_GROUP_STOP(stabilizer)

LOG_GROUP_START(acc) LOG_ADD(LOG_FLOAT, x, &acc.x)
LOG_ADD(LOG_FLOAT, y, &acc.y)
LOG_ADD(LOG_FLOAT, z, &acc.z)
LOG_ADD(LOG_FLOAT, zw, &accWZ)
LOG_ADD(LOG_FLOAT, mag2, &accMAG)
LOG_GROUP_STOP(acc)

LOG_GROUP_START(gyro) LOG_ADD(LOG_FLOAT, x, &gyro.x)
LOG_ADD(LOG_FLOAT, y, &gyro.y)
LOG_ADD(LOG_FLOAT, z, &gyro.z)
LOG_GROUP_STOP(gyro)

LOG_GROUP_START(mag) LOG_ADD(LOG_FLOAT, x, &mag.x)
LOG_ADD(LOG_FLOAT, y, &mag.y)
LOG_ADD(LOG_FLOAT, z, &mag.z)
LOG_GROUP_STOP(mag)

LOG_GROUP_START(motor) LOG_ADD(LOG_INT32, m4, &motorPowerM4)
LOG_ADD(LOG_INT32, m1, &motorPowerM1)
LOG_ADD(LOG_INT32, m2, &motorPowerM2)
LOG_ADD(LOG_INT32, m3, &motorPowerM3)
LOG_GROUP_STOP(motor)

// LOG altitude hold PID controller states
LOG_GROUP_START(vpid) LOG_ADD(LOG_FLOAT, pid, &altHoldPID)
LOG_ADD(LOG_FLOAT, p, &altHoldPID.outP)
LOG_ADD(LOG_FLOAT, i, &altHoldPID.outI)
LOG_ADD(LOG_FLOAT, d, &altHoldPID.outD)
LOG_GROUP_STOP(vpid)

LOG_GROUP_START(baro) LOG_ADD(LOG_FLOAT, asl, &asl)
LOG_ADD(LOG_FLOAT, aslRaw, &aslRaw)
LOG_ADD(LOG_FLOAT, aslLong, &aslLong)
LOG_ADD(LOG_FLOAT, temp, &temperature)
LOG_ADD(LOG_FLOAT, pressure, &pressure)
LOG_GROUP_STOP(baro)

LOG_GROUP_START(altHold) LOG_ADD(LOG_FLOAT, err, &altHoldErr)
LOG_ADD(LOG_FLOAT, target, &altHoldTarget)
LOG_ADD(LOG_FLOAT, zSpeed, &vSpeed)
LOG_ADD(LOG_FLOAT, vSpeed, &vSpeed)
LOG_ADD(LOG_FLOAT, vSpeedASL, &vSpeedASL)
LOG_ADD(LOG_FLOAT, vSpeedAcc, &vSpeedAcc)
LOG_GROUP_STOP(altHold)

// Params for altitude hold
PARAM_GROUP_START(altHold) PARAM_ADD(PARAM_FLOAT, aslAlpha, &aslAlpha)
PARAM_ADD(PARAM_FLOAT, aslAlphaLong, &aslAlphaLong)
PARAM_ADD(PARAM_FLOAT, errDeadband, &errDeadband)
PARAM_ADD(PARAM_FLOAT, altHoldChangeSens, &altHoldChange_SENS)
PARAM_ADD(PARAM_FLOAT, altHoldErrMax, &altHoldErrMax)
PARAM_ADD(PARAM_FLOAT, kd, &altHoldKd)
PARAM_ADD(PARAM_FLOAT, ki, &altHoldKi)
PARAM_ADD(PARAM_FLOAT, kp, &altHoldKp)
PARAM_ADD(PARAM_FLOAT, pidAlpha, &pidAlpha)
PARAM_ADD(PARAM_FLOAT, pidAslFac, &pidAslFac)
PARAM_ADD(PARAM_FLOAT, vAccDeadband, &vAccDeadband)
PARAM_ADD(PARAM_FLOAT, vBiasAlpha, &vBiasAlpha)
PARAM_ADD(PARAM_FLOAT, vSpeedAccFac, &vSpeedAccFac)
PARAM_ADD(PARAM_FLOAT, vSpeedASLDeadband, &vSpeedASLDeadband)
PARAM_ADD(PARAM_FLOAT, vSpeedASLFac, &vSpeedASLFac)
PARAM_ADD(PARAM_FLOAT, vSpeedLimit, &vSpeedLimit)
PARAM_ADD(PARAM_UINT16, baseThrust, &altHoldBaseThrust)
PARAM_ADD(PARAM_UINT16, maxThrust, &altHoldMaxThrust)
PARAM_ADD(PARAM_UINT16, minThrust, &altHoldMinThrust)
PARAM_GROUP_STOP(altHold)

#ifdef ADAPTIVE_CONTROLLER
LOG_GROUP_START(height)
LOG_ADD(LOG_FLOAT, heightMeasured, &heightActual)
LOG_ADD(LOG_FLOAT, heightEstimated, &estHeight)
LOG_ADD(LOG_FLOAT, velocityEstimated, &estVelocity)
LOG_ADD(LOG_FLOAT, accMeasured, &accWZ)
LOG_GROUP_STOP(height)
#endif
