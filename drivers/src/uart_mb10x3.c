/*
 * uart_mb10x3.c - HRLV-MaxSonar-EZ ultra-sound range finder using the RS323 output.
 *
 *  Created on: Nov 28, 2015
 *      Author: Martin Chung
 */

#include "uart_mb10x3.h"
#include <math.h>
#include <string.h>
/*ST includes */
#include "stm32fxxx.h"
/*FreeRtos includes*/
#include "FreeRTOS.h"
#include "task.h"
//#include "semphr.h"
#include "queue.h"
#include "crtp.h"
#include "cfassert.h"
#include "nvicconf.h"
#include "config.h"
#include "log.h"

#define UARTn_DATA_TIMEOUT_MS 1000
#define UARTn_DATA_TIMEOUT_TICKS (UARTn_DATA_TIMEOUT_MS / portTICK_RATE_MS)
#define CCR_ENABLE_SET  ((uint32_t)0x00000001)
#define MESSAGE_LENGTH 5
#define MB10X3_UPDATE_FREQ 10

static bool isInit = false;

//xSemaphoreHandle MB10X3WaitUntilSendDone = NULL;
static xQueueHandle MB10X3UartDataDelivery;
static bool startReceiving = false;

static float range = 0; // range in meters.
static char msg[6] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void uartMB10X3RxTask(void *param);

void uartMB10X3Init(void) {

	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable GPIO and USART clock */
	RCC_AHB1PeriphClockCmd(UARTn_GPIO_PERIF, ENABLE); //uart4-5, usart2-3
	ENABLE_UARTn_RCC(UARTn_PERIF, ENABLE);

	/* Configure USART Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = UARTn_GPIO_RX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(UARTn_GPIO_PORT, &GPIO_InitStructure);

	/* Configure USART Tx as alternate function */
	GPIO_InitStructure.GPIO_Pin = UARTn_GPIO_TX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(UARTn_GPIO_PORT, &GPIO_InitStructure);

	//Map uart to alternate functions
	//GPIO_PinAFConfig(UARTn_GPIO_PORT, UARTn_GPIO_AF_TX_PIN, UARTn_GPIO_AF_TX);
	GPIO_PinAFConfig(UARTn_GPIO_PORT, UARTn_GPIO_AF_RX_PIN, UARTn_GPIO_AF_RX);

	USART_InitStructure.USART_BaudRate = 9600; //= 1000000;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
#ifdef UART_SPINLOOP_FLOWCTRL
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_CTS;
#else
	USART_InitStructure.USART_HardwareFlowControl =
			USART_HardwareFlowControl_None;
#endif
	USART_Init(UARTn_TYPE, &USART_InitStructure);

	// TODO: Enable
	// Configure Tx buffer empty interrupt
	NVIC_InitStructure.NVIC_IRQChannel = UARTn_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = NVIC_UART_PRI;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	//vSemaphoreCreateBinary(MB10X3WaitUntilSendDone);
	MB10X3UartDataDelivery = xQueueCreate(6, sizeof(uint8_t));

	USART_ITConfig(UARTn_TYPE, USART_IT_RXNE, ENABLE);

	xTaskCreate(uartMB10X3RxTask,
			(const signed char * const )UART_MB10X3_RX_TASK_NAME,
			UART_MB10X3_RX_TASK_STACKSIZE, NULL, UART_MB10X3_RX_TASK_PRI, NULL);

	//Enable UART
	USART_Cmd(UARTn_TYPE, ENABLE);

	isInit = true;
}

bool uartMB10X3Test(void) {
	return isInit;
}

static char uartMB10X3Getc() {
	char c;
	if (xQueueReceive(MB10X3UartDataDelivery, &c, 0) != pdTRUE)
		c = 0x00;
	return c;
}

float mb10X3GetRange(void) {
	return range;
}

static void uartMB10X3ParseMsg(void) {
	uint8_t c;
	int index = 0;
	float result = 0.0f; // value of message in mm.
	bool parseSuccessful = true;
	int digit = 0;
	c = uartMB10X3Getc();
	msg[0] = c;
	//R must be the first character in the queue.
	if (c != 0x52)
		parseSuccessful = false;
	while (index < MESSAGE_LENGTH - 1 && parseSuccessful) {
		c = uartMB10X3Getc();
		msg[index + 1] = c;
		// cr must be the 6th character in the queue.
		//if (index == 4 && c != 0x0d)
		//	parseSuccessful = false;
		switch (c) {
		case 0x00: //null
			digit = 0;
			parseSuccessful = false;
			break;
		case 0x0d: //cr must be the 6th character in the queue.
			digit = 0;
			if (index != 4)
				parseSuccessful = false;
			break;
		case 0x30: //0
			digit = 0;
			break;
		case 0x31: //1
			digit = 1;
			break;
		case 0x32: //2
			digit = 2;
			break;
		case 0x33: //3
			digit = 3;
			break;
		case 0x34: //4
			digit = 4;
			break;
		case 0x35: //5
			digit = 5;
			break;
		case 0x36: //6
			digit = 6;
			break;
		case 0x37: //7
			digit = 7;
			break;
		case 0x38: //8
			digit = 8;
			break;
		case 0x39: //9
			digit = 9;
			break;
		default:
			digit = 0;
			parseSuccessful = false;
			break;
		}
		result += digit * pow(10, 3 - index);
		index++;
	}
	if (parseSuccessful) {
		range = result / 1000; // range in meters.
	}
}

void uartMB10X3RxTask(void *param) {
	uint32_t lastWakeTime;
	lastWakeTime = xTaskGetTickCount();
	while (1) {
		vTaskDelayUntil(&lastWakeTime, F2T(MB10X3_UPDATE_FREQ)); // 20Hz
		uartMB10X3ParseMsg();
	}
}

void uartMB10X3Isr(void) {
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	uint8_t rxDataInterrupt;

	/*	if (USART_GetITStatus(UARTn_TYPE, USART_IT_TXE)) {
	 if (outDataIsr && (dataIndexIsr < dataSizeIsr)) {
	 USART_SendData(UARTn_TYPE, outDataIsr[dataIndexIsr] & 0x00FF);
	 dataIndexIsr++;
	 } else {
	 USART_ITConfig(UARTn_TYPE, USART_IT_TXE, DISABLE);
	 xHigherPriorityTaskWoken = pdFALSE;
	 xSemaphoreGiveFromISR(MB10X3WaitUntilSendDone,
	 &xHigherPriorityTaskWoken);
	 }
	 }
	 USART_ClearITPendingBit(UARTn_TYPE, USART_IT_TXE);
	 */
	if (USART_GetITStatus(UARTn_TYPE, USART_IT_RXNE)) {
		rxDataInterrupt = USART_ReceiveData(UARTn_TYPE) & 0x00FF;
		//We flush the queue when the 'R' character is received. The 'R' character is then inserted into the queue.
		//if (rxDataInterrupt == 0x52) {
			startReceiving = true;
			//xQueueReset(MB10X3UartDataDelivery);
		//}
		//else if (rxDataInterrupt == 0x0d)
		//{
		//	startReceiving = false;
		//}
		if (startReceiving)
			msg[0] = rxDataInterrupt;
			//xQueueSendFromISR(MB10X3UartDataDelivery, &rxDataInterrupt,
			//		&xHigherPriorityTaskWoken);
		//USART_ClearITPendingBit(UARTn_TYPE, USART_IT_RXNE);
	}
}

LOG_GROUP_START(rangeSensor) LOG_ADD(LOG_FLOAT, range, &range)
LOG_ADD(LOG_UINT8, msg0, msg)
LOG_ADD(LOG_UINT8, msg1, &(msg[1]))
LOG_ADD(LOG_UINT8, msg2, &(msg[2]))
LOG_ADD(LOG_UINT8, msg3, &(msg[3]))
LOG_ADD(LOG_UINT8, msg4, &(msg[4]))
//LOG_ADD(LOG_UINT8, msg5, &(msg[5]))
LOG_GROUP_STOP(rangeSensor)
