/*
 * vl6180x.h
 *
 *  Created on: Dec 18, 2015
 *      Author: Martin Chung
 */

#ifndef VL6180_H_
#define VL6180_H_

#include "stdbool.h"
#include "i2cdev.h"

/*enum {
	SINGLE_SHOT_RANGE = 0,
	SINGLE_SHOT_ALS = 1,
	CONTINUOUS_RANGE = 2,
	CONTINUOUS_ALS = 3,
	SINGLE_ALS_CONTINUOUS_RANGE = 4,
	SINGLE_RANGE_CONTINUOUS_ALS = 5,
	INTERLEAVED = 6
} VL6180X_MODES;
*/
void vl6180xInit(void);
bool vl6180xSelfTest(void);
//bool vl6180xSetMode(VL6180X_MODES mode);
float vl6180GetRange(void);
//int32_t vl6180GetAmbientLight(float gain, float integrationTime);


/**
 * @brief       Write data buffer to VL6180x device via i2c
 * @param dev   The device to write to
 * @param buff  The data buffer
 * @param len   The length of the transaction in byte
 * @return      0 on success
 * @ingroup cci_i2c
 */
int  VL6180x_I2CWrite(uint8_t dev, uint8_t  *buff, uint8_t len);

/**
 *
 * @brief       Read data buffer from VL6180x device via i2c
 * @param dev   The device to read from
 * @param buff  The data buffer to fill
 * @param len   The length of the transaction in byte
 * @return      0 on success
 * @ingroup  cci_i2c
 */
int VL6180x_I2CRead(uint8_t dev, uint8_t *buff, uint8_t len);

#endif /* VL6180_H_ */
