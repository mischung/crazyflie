#!/usr/bin/env python2.7

import time
from threading import Thread
import logging
import os
import os.path as _path
from os.path import expanduser
from cflib.crazyflie.mem import MemoryElement
import sys
import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import Log, LogVariable, LogConfig

MAG_GAUSS_PER_LSB = 666.7

logger = logging.getLogger('')
class Main:
    def __init__(self):
	logging.basicConfig(level=logging.INFO)
	filehandler = logging.FileHandler('log.log')
        filehandler.setLevel(logging.INFO)
        logger.addHandler(filehandler)
        self._stop = 0
        self.crazyflie = Crazyflie()
        cflib.crtp.init_drivers()

        # You may need to update this value if your Crazyradio uses a different frequency.
        self.crazyflie.open_link("radio://0/80/250K")
        self.crazyflie.connected.add_callback(self.connectSetupFinished)
 
    def connectSetupFinished(self, linkURI):
	logger.info("Crazyflie Connected.")
        lg = LogConfig("Magnetometer", 100)
        lg.add_variable("mag_raw.x", "int16_t")
        lg.add_variable("mag_raw.y", "int16_t")
        lg.add_variable("mag_raw.z", "int16_t")
        self.crazyflie.log.add_config(lg)    
        #log = self.crazyflie.log.create_log_packet(lg)
	lg.data_received_cb.add_callback(self.magData)
        lg.start()
        
        # Keep the commands alive so the firmware kill-switch doesn't kick in.
        Thread(target=self.pulse_command).start()
        Thread(target=self.input_loop).start()

    def stop(self):
        # Exit command received
        # Set thrust to zero to make sure the motors turn off NOW

        # make sure we actually set the thrust to zero before we kill the thread that sets it
        time.sleep(0.5) 
        self._stop = 1;

        # Exit the main loop
        time.sleep(1)
        self.crazyflie.close_link() # This errors out for some reason. Bad libusb?
        sys.exit(0)

    def input_loop(self):
        command = raw_input("")
        self.stop()
    
    def pulse_command(self):
        while 1:
            self.crazyflie.commander.send_setpoint(0, 0, 0, 0)
            time.sleep(0.1)
     
            # Exit if the parent told us to
            if self._stop==1:
                return
 
    def magData(self, timestamp, data, logconf):
        x, y, z = data['mag_raw.x'], data['mag_raw.y'], data['mag_raw.z']
        try:
            print x, y, z
            sys.stdout.flush()
        except:
            self.stop()
 
Main()
