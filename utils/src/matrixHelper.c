/*
 * matrixHelper.c
 *
 *  Created on: Jan 4, 2016
 *      Author: Martin Chung
 */
#include "matrixHelper.h"
#include <stdbool.h>
#include "arm_math.h"
#include "cfassert.h"
#include "FreeRTOS.h"

void assignMatrix(const arm_matrix_instance_f32 *from,
		arm_matrix_instance_f32 *to) {
	int i;
	int j;
	int index;
	index = 0;
	ASSERT(from->numCols == to->numCols && from->numRows == to->numRows);
	for (j = 0; j < from->numRows; j++)
		for (i = 0; i < from->numCols; i++) {
			to->pData[index] = from->pData[index];
			index++;
		}
}

void copyAndInitMatrix(const arm_matrix_instance_f32 *source,
		arm_matrix_instance_f32 *dst) {
	size_t size = source->numRows * source->numCols * sizeof(float32_t);
	float32_t *data = (float32_t *) pvPortMalloc(size);
	MATRIX_INIT(*dst, source->numRows, source->numCols, data);
	ASSIGN_MATRIX(*source, *dst);
}

void clampMatrix(const arm_matrix_instance_f32 *source,
		const arm_matrix_instance_f32 *min, const arm_matrix_instance_f32 *max,
		const arm_matrix_instance_f32 *dst) {
	ASSERT(
			source->numCols == min->numCols && source->numCols == max->numCols
					&& source->numCols == dst->numCols
					&& source->numRows == min->numRows
					&& source->numRows == max->numRows
					&& source->numRows == dst->numRows);
	int i;
	int j;
	int k;

	k = 0;
	for (j = 0; j < source->numCols; j++) {
		for (i = 0; i < source->numRows; i++) {
			dst->pData[k] = CLAMP(source->pData[k], min->pData[k],
					max->pData[k]);
			k = k + 1;
		}
	}
}

arm_status invertMatrixAndPreserveSource(const arm_matrix_instance_f32 * pSrc,
		arm_matrix_instance_f32 * pDst) {
	arm_status status;
	arm_matrix_instance_f32 temp;
	copyAndInitMatrix(pSrc, &temp);
	status = arm_mat_inverse_f32(&temp, pDst);
	vPortFree(temp.pData);
	return status;
}

float r8mat_norm_li(const arm_matrix_instance_f32 *a)

/******************************************************************************/
/*
 Purpose:

 R8MAT_NORM_LI returns the matrix L-oo norm of an R8MAT.

 Discussion:

 An R8MAT is a doubly dimensioned array of R8 values, stored as a vector
 in column-major order.

 The matrix L-oo norm is defined as:

 R8MAT_NORM_LI =  max ( 1 <= I <= M ) sum ( 1 <= J <= N ) abs ( A(I,J) ).

 The matrix L-oo norm is derived from the vector L-oo norm,
 and satisifies:

 r8vec_norm_li ( A * x ) <= r8mat_norm_li ( A ) * r8vec_norm_li ( x ).

 Licensing:

 This code is distributed under the GNU LGPL license.

 Modified:

 01 December 2011

 Author:

 John Burkardt

 Parameters:

 Input, int M, the number of rows in A.

 Input, int N, the number of columns in A.

 Input, double A[M*N], the matrix whose L-oo
 norm is desired.

 Output, double R8MAT_NORM_LI, the L-oo norm of A.
 */
{
	int i;
	int j;
	float32_t row_sum;
	float32_t value;

	value = 0.0;

	for (i = 0; i < a->numRows; i++) {
		row_sum = 0.0;
		for (j = 0; j < a->numCols; j++) {
			row_sum = row_sum + fabs(a->pData[i * a->numCols + j]);
		}
		value = MAX(value, row_sum);
	}
	return value;
}

void r8mat_add(float alpha, const arm_matrix_instance_f32 *a, float beta,
		const arm_matrix_instance_f32 *b, arm_matrix_instance_f32 *c)
/*
 Purpose:

 R8MAT_ADD computes C = alpha * A + beta * B for R8MAT's.

 Discussion:

 An R8MAT is a doubly dimensioned array of R8 values, stored as a vector
 in column-major order.

 Thanks to Kjartan Halvorsen for pointing out an error, 09 November 2012.

 Licensing:

 This code is distributed under the GNU LGPL license.

 Modified:

 09 November 2012

 Author:

 John Burkardt

 Parameters:

 Input, int M, N, the number of rows and columns.

 Input, double ALPHA, the multiplier for A.

 Input, double A[M*N], the first matrix.

 Input, double BETA, the multiplier for A.

 Input, double B[M*N], the second matrix.

 Output, double C[M*N], the sum of alpha*A+beta*B.
 */
{
	int i;
	int j;

	for (j = 0; j < a->numCols; j++) {
		for (i = 0; i < a->numRows; i++) {
			c->pData[i * a->numCols + j] = alpha * a->pData[i * a->numCols + j]
					+ beta * b->pData[i * a->numCols + j];
		}
	}
	return;
}

void r8mat_identity_new(int n, arm_matrix_instance_f32 *dst)
/*
 Purpose:

 R8MAT_IDENTITY_NEW returns an identity matrix.

 Discussion:

 An R8MAT is a doubly dimensioned array of R8 values, stored as a vector
 in column-major order.

 Licensing:

 This code is distributed under the GNU LGPL license.

 Modified:

 06 September 2005

 Author:

 John Burkardt

 Parameters:

 Input, int N, the order of A.

 Output, double R8MAT_IDENTITY_NEW[N*N], the N by N identity matrix.
 */
{
	float32_t *a;
	int i;
	int j;
	int k;

	a = (float32_t *) pvPortMalloc(n * n * sizeof(float32_t));

	k = 0;
	for (j = 0; j < n; j++) {
		for (i = 0; i < n; i++) {
			if (i == j) {
				a[k] = 1.0f;
			} else {
				a[k] = 0.0f;
			}
			k = k + 1;
		}
	}
	MATRIX_INIT(*dst, n, n, a);
}

float r8_log_2(float x)

/*
 Purpose:

 R8_LOG_2 returns the logarithm base 2 of an R8.

 Discussion:

 R8_LOG_2 ( X ) = Log ( |X| ) / Log ( 2.0 )

 Licensing:

 This code is distributed under the GNU LGPL license.

 Modified:

 12 May 2006

 Author:

 John Burkardt

 Parameters:

 Input, double X, the number whose base 2 logarithm is desired.
 X should not be 0.

 Output, double R8_LOG_2, the logarithm base 2 of the absolute
 value of X.  It should be true that |X| = 2^R_LOG_2.
 */
{
	float value;

	if (x == 0.0) {
		value = -r8_big();
	} else {
		value = log(fabs(x)) / log(2.0);
	}

	return value;
}

float r8_big()

/******************************************************************************/
/*
 Purpose:

 R8_BIG returns a "big" R8.

 Discussion:

 The value returned by this function is NOT required to be the
 maximum representable R8.  We simply want a "very large" but
 non-infinite number.

 Licensing:

 This code is distributed under the GNU LGPL license.

 Modified:

 27 September 2014

 Author:

 John Burkardt

 Parameters:

 Output, double R8_BIG, a "big" R8 value.
 */
{
	float value;

	value = 1.0E+30;

	return value;
}

void expMat(const arm_matrix_instance_f32 *a, arm_matrix_instance_f32 *dst)
/*
 Purpose:

 R8MAT_EXPM1 is essentially MATLAB's built-in matrix exponential algorithm.

 Licensing:

 This code is distributed under the GNU LGPL license.

 Modified:

 01 December 2011

 Author:

 Cleve Moler, Charles Van Loan

 Reference:

 Cleve Moler, Charles VanLoan,
 Nineteen Dubious Ways to Compute the Exponential of a Matrix,
 Twenty-Five Years Later,
 SIAM Review,
 Volume 45, Number 1, March 2003, pages 3-49.

 Parameters:

 Input, int N, the dimension of the matrix.

 Input, double A[N*N], the matrix.

 Output, double R8MAT_EXPM1[N*N], the estimate for exp(A).
 */
{
	arm_matrix_instance_f32 a2;
	float a_norm;
	float c;
	arm_matrix_instance_f32 d;
	arm_matrix_instance_f32 invD;
	arm_matrix_instance_f32 e;
	arm_matrix_instance_f32 tmpE;
	arm_matrix_instance_f32 tmpE2;
	IDENTITY_MATRIX_NEW(a->numCols, invD);
	int ee;
	int k;
	const float one = 1.0f;
	int p;
	const int q = 6;
	int s;
	float t;
	arm_matrix_instance_f32 x;
	arm_matrix_instance_f32 tmpX;

	COPY_NEW_MATRIX(*a, a2);
	a_norm = r8mat_norm_li(&a2);
	ee = (int) (r8_log_2(a_norm)) + 1;
	s = MAX(0, ee + 1);
	t = 1.0 / pow(2.0, s);
	MATRIX_SCALE(a2, t, a2);
	COPY_NEW_MATRIX(a2, x);
	c = 0.5f;
	IDENTITY_MATRIX_NEW(a->numCols, e);
	r8mat_add(one, &e, c, &a2, &e);
	IDENTITY_MATRIX_NEW(a->numCols, d);
	r8mat_add(one, &d, -c, &a2, &d);
	p = 1;
	for (k = 2; k <= q; k++) {
		c = c * (float) (q - k + 1) / (float) (k * (2 * q - k + 1));
		COPY_NEW_MATRIX(x, tmpX);
		MATRIX_MULT(a2, tmpX, x);

		r8mat_add(c, &x, one, &e, &e);
		if (p) {
			r8mat_add(c, &x, one, &d, &d);
		} else {
			r8mat_add(-c, &x, one, &d, &d);
		}
		p = !p;
	}
	/*
	 E -> inverse(D) * E
	 */
	MATRIX_INVERSE(d, invD);
	COPY_NEW_MATRIX(e, tmpE);
	MATRIX_MULT(invD, tmpE, e);
	/*
	 E -> E^(2*S)
	 */
	for (k = 1; k <= s; k++) {
		COPY_NEW_MATRIX(e, tmpE);
		COPY_NEW_MATRIX(e, tmpE2);
		MATRIX_MULT(tmpE, tmpE2, e);
	}
	ASSIGN_MATRIX(e, *dst);

	vPortFree(a2.pData);
	vPortFree(d.pData);
	vPortFree(x.pData);
	vPortFree(invD.pData);
	vPortFree(tmpX.pData);
	vPortFree(tmpE.pData);
	vPortFree(tmpE2.pData);
	vPortFree(e.pData);
}






